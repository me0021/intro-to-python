import random
rollInput = input("Roll a dice in the format roll # d # (with spaces).\n")
if len(rollInput.split(" ")) == 4:
    x, numDice, y, numSides = rollInput.split(" ")
    if 'roll' in x and 'd' in y and numDice.isnumeric() and numSides.isnumeric():
        total = 0
        for i in range(int(numDice)):
            total += random.randint(1, int(numSides))
        print(total)
    else:
        print("Incorrect format!")
else:
    print("Incorrect format!")