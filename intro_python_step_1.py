# must input as roll 1 d 6 (with spaces)
import random
rollInput = input()
x, numDice, y, numSides = rollInput.split(" ")
total = 0
for i in range(int(numDice)):
    total = total + random.randint(1, int(numSides))
print(total)