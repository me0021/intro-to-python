import random
# must input as roll 1 d 6 with spaces between
# Add a prompt
rollInput = input("Roll a dice in the format roll # d # (with spaces).\n")
# Check if they input the correct number of argments
if len(rollInput.split(" ")) == 4:
    #split out
    x, numDice, y, numSides = rollInput.split(" ")
    # check if the values match
    if 'roll' in x and 'd' in y and numDice.isnumeric() and numSides.isnumeric():
        # only now perform any calculations
        total = 0
        for i in range(int(numDice)):
            total += random.randint(1, int(numSides))
        print(total)
    else:
        print("Incorrect format!")
else:
    print("Incorrect format!")